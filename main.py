from typing import Optional

from fastapi import FastAPI, Response

app = FastAPI()


@app.get("/")
async def root(str: Optional[str] = None):
        if str is None:
                return Response(content="Nothing to echo :(", media_type="text/plain")
        else:
                return Response(content=str, media_type="text/plain")
