FROM ubuntu
RUN apt-get update && apt-get install
RUN apt install -y python3 pip
RUN pip install fastapi uvicorn
COPY main.py .
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8080"]
